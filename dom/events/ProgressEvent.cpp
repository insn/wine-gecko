/* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim: set ts=8 sts=2 et sw=2 tw=80: */
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "mozilla/dom/ProgressEvent.h"
#include "mozilla/dom/ProgressEventBinding.h"

#include "jsapi.h"

namespace mozilla {
namespace dom {

NS_IMPL_CYCLE_COLLECTION_CLASS(ProgressEvent)

NS_IMPL_CYCLE_COLLECTION_TRAVERSE_BEGIN_INHERITED(ProgressEvent, Event)
NS_IMPL_CYCLE_COLLECTION_TRAVERSE_END

NS_IMPL_CYCLE_COLLECTION_TRACE_BEGIN_INHERITED(ProgressEvent, Event)
NS_IMPL_CYCLE_COLLECTION_TRACE_END

NS_IMPL_CYCLE_COLLECTION_UNLINK_BEGIN_INHERITED(ProgressEvent, Event)
NS_IMPL_CYCLE_COLLECTION_UNLINK_END

NS_INTERFACE_MAP_BEGIN_CYCLE_COLLECTION_INHERITED(ProgressEvent)
  NS_INTERFACE_MAP_ENTRY(nsIDOMProgressEvent)
NS_INTERFACE_MAP_END_INHERITING(Event)

NS_IMPL_ADDREF_INHERITED(ProgressEvent, Event)
NS_IMPL_RELEASE_INHERITED(ProgressEvent, Event)

ProgressEvent::ProgressEvent(EventTarget* aOwner)
  : Event(aOwner, nullptr, nullptr)
{
}

ProgressEvent::~ProgressEvent()
{
}

ProgressEvent*
ProgressEvent::AsProgressEvent()
{
  return this;
}

JSObject*
ProgressEvent::WrapObjectInternal(JSContext* aCx, JS::Handle<JSObject*> aGivenProto)
{
  return ProgressEventBinding::Wrap(aCx, this, aGivenProto);
}

already_AddRefed<ProgressEvent>
ProgressEvent::Constructor(EventTarget* aOwner, const nsAString& aType, const ProgressEventInit& aEventInitDict)
{
  RefPtr<ProgressEvent> e = new ProgressEvent(aOwner);
  bool trusted = e->Init(aOwner);
  e->InitEvent(aType, aEventInitDict.mBubbles, aEventInitDict.mCancelable);
  e->mLengthComputable = aEventInitDict.mLengthComputable;
  e->mLoaded = aEventInitDict.mLoaded;
  e->mTotal = aEventInitDict.mTotal;
  e->SetTrusted(trusted);
  return e.forget();
}

already_AddRefed<ProgressEvent>
ProgressEvent::Constructor(const GlobalObject& aGlobal, const nsAString& aType, const ProgressEventInit& aEventInitDict, ErrorResult& aRv)
{
  nsCOMPtr<EventTarget> owner = do_QueryInterface(aGlobal.GetAsSupports());
  return Constructor(owner, aType, aEventInitDict);
}

bool
ProgressEvent::LengthComputable() const
{
  return mLengthComputable;
}

uint64_t
ProgressEvent::Loaded() const
{
  return mLoaded;
}

uint64_t
ProgressEvent::Total() const
{
  return mTotal;
}

NS_IMETHODIMP
ProgressEvent::GetLengthComputable(bool* aLengthComputable)
{
  *aLengthComputable = mLengthComputable;
  return NS_OK;
}

NS_IMETHODIMP
ProgressEvent::GetLoaded(uint64_t* aLoaded)
{
  *aLoaded = mLoaded;
  return NS_OK;
}

NS_IMETHODIMP
ProgressEvent::GetTotal(uint64_t* aTotal)
{
  *aTotal = mTotal;
  return NS_OK;
}

NS_IMETHODIMP
ProgressEvent::InitProgressEvent(const nsAString& aType,
                                 bool aCanBubble,
                                 bool aCancelable,
                                 bool aLengthComputable,
                                 uint64_t aLoaded,
                                 uint64_t aTotal)
{
  Event::InitEvent(aType, aCanBubble, aCancelable);

  mLoaded = aLoaded;
  mLengthComputable = aLengthComputable;
  mTotal = aTotal;

  return NS_OK;
}

} // namespace dom
} // namespace mozilla
